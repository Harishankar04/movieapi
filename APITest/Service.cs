﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APITest
{
    public class Service
    {
        string Baseurl = "https://fakestoreapi.com/";
        public RestResponse GET()
        {
            RestClient client = new RestClient(Baseurl);
            RestRequest request = new RestRequest("products",Method.Get);
            RestResponse response = new RestResponse();
            response = client.Execute(request);
            return response;
        }
        public RestResponse POST()
        {
            RestClient client = new RestClient(Baseurl);
            RestRequest request = new RestRequest("products", Method.Post);
            string Json = "{\"id\": 1,\"title\": \"Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops\",\"price\": 109.95,\"description\": \"Your perfect pack for everyday use and walks in the forest. Stash your laptop (up to 15 inches) in the padded sleeve, your everyday\",\"category\": \"men's clothing\",\"image\": \"https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg\",\"rating\": {\"rate\": 3.9,\"count\": 120}}";
            request.AddBody(Json);
            RestResponse response = client.Execute(request);
            return response;
        }
        public RestResponse PUT()
        {
            RestClient client = new RestClient(Baseurl);
            RestRequest request = new RestRequest("products/2", Method.Put);
            string json = "{\"id\": 1,\"title\": \"Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops\",\"price\": 109.95,\"description\": \"Your perfect pack for everyday use and walks in the forest. Stash your laptop (up to 15 inches) in the padded sleeve, your everyday\",\"category\": \"men's clothing\",\"image\": \"https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg\",\"rating\": {\"rate\": 3.9,\"count\": 120}}";
            request.AddBody(json);
            RestResponse response = client .Execute(request);
            return response;
        }
        public RestResponse PATCH()
        {
            RestClient client = new RestClient(Baseurl);
            RestRequest request = new RestRequest("products/4", Method.Patch);
            string json = "{\"id\": 1,\"title\": \"Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops\",\"price\": 109.95,\"description\": \"Your perfect pack for everyday use and walks in the forest. Stash your laptop (up to 15 inches) in the padded sleeve, your everyday\",\"category\": \"men's clothing\",\"image\": \"https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg\",\"rating\": {\"rate\": 3.9,\"count\": 120}}";
            request.AddBody(json);
            RestResponse response = client.Execute(request);
            return response;
        }
        public RestResponse DELETE()
        {
            RestClient client = new RestClient(Baseurl);
            RestRequest request = new RestRequest("products/3", Method.Delete);
            RestResponse response = client.Execute(request);
            return response;
        }
    }
}
