using Newtonsoft.Json;
using RestSharp;
using System.Net;

namespace APITest
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestGET()
        {
            Service S = new Service();
            RestResponse response = S.GET();
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK,response.StatusCode);

            Assert.AreEqual(response.ContentType,"application/json");

            //val res = JsonConvert.DeserializeObject<val>(response.Content);
            //Assert.AreEqual("Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops", res.title);
            //List<val> res = JsonConvert.DeserializeObject<List<val>>(response.Content);
            //Assert.AreEqual("Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops", res[0].title);
        }
        [Test]
        public void TestPost()
        {
            Service S = new Service();
            RestResponse response = S.POST();
            Assert.IsNotNull(response);
            Assert.AreEqual (HttpStatusCode.OK,response.StatusCode);
        }
        [Test]
        public void TestPUT()
        {
            Service S = new Service();
            RestResponse response = S.PUT();
            Assert.IsNotNull(response);
            Assert.AreEqual(response.StatusCode,HttpStatusCode.OK);
        }
        [Test]
        public void TestPATCH()
        {
            Service S = new Service();
            RestResponse response = S.PATCH();
            Assert.IsNotNull(response);
            Assert.AreEqual(response.StatusCode,HttpStatusCode.OK);
        }
        [Test]
        public void TestDELETE()
        {
            Service S = new Service ();
            RestResponse response = S.DELETE();
            Assert.IsNotNull(response);
            Assert.AreEqual(response.StatusCode,HttpStatusCode.OK);
        }
    }   
}