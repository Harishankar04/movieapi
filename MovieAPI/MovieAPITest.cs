using Newtonsoft.Json;
using RestSharp;
using System.Net;

namespace MovieAPI
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestGet()
        {
            MovieClass MC = new MovieClass();
            RestResponse response = MC.GET();
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Console.WriteLine(response.Content.ToString());

            /*property validate = JsonConvert.DeserializeObject<property>(response.Content);
            Assert.AreEqual("The Lord of the Rings Series", validate.name);*/
        }
    }
}